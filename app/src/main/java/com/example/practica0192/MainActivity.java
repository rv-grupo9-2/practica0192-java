package com.example.practica0192;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnPulsar;

    private Button btnPulsar2;

    private Button btnPulsar3;
    private EditText txtNombre;
    private TextView lblSaludar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // relacionar los objetos
        btnPulsar = (Button) findViewById(R.id.btnSaludar);
        btnPulsar2 = (Button) findViewById(R.id.btnLimpiar);
        btnPulsar3 = (Button) findViewById(R.id.btnCerrar);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        lblSaludar = (TextView) findViewById(R.id.lblSaludo);

        //codificar el evento clic del boton para imprimir texto
        btnPulsar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //validar
                if(txtNombre.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this,
                            "Falto capturar informacion",
                            Toast.LENGTH_SHORT).show();
                } else {
                    String str = "Hola "
                            + txtNombre.getText().toString() + " ¿como estas?";
                    lblSaludar.setText(str.toString());
                }
            }
        });

        //codificar el evento clic del boton para limpiar texto
        btnPulsar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //validar
                if (lblSaludar.getText().toString().matches(":: ::")) {
                    Toast.makeText(MainActivity.this,
                            "No hay informacion para limpiar",
                            Toast.LENGTH_SHORT).show();
                } else {
                    String str = ":: ::";
                    lblSaludar.setText(str.toString());
                }
            }
        });

        //codificar el evento clic del boton para cerrar la app
        btnPulsar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //funcion para cerrar aplicacion
                finish();
            }
        });

    }
}